package com.lyc.pojo;

public class FaceModelScore extends FaceModel{

    private Float score;

    public Float getScore() {
        return score;
    }

    public void setScore(Float score) {
        this.score = score;
    }
}
